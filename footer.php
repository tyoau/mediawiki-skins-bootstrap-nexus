

			<!-- Ad here maybe -->
			    <?php
			      $footerLinks = $this->getFooterLinks();
			
			      if (is_array($footerLinks)) {
			        foreach($footerLinks as $category => $links ):
			          if ($category === 'info') { 
					?>
					<div class="row">
					<ul class="list-unstyled" id="footer-<?php
						echo $category
						?>">
							<?php
							foreach ( $links as $link ) {
								?>
								<li id="footer-<?php
								echo $category
								?>-<?php
								echo $link
								?>"><?php
									$this->html( $link )
									?></li>
							<?php
							}
							?>
						</ul>
					</div>
					<?php 
					  } 
						else {
			        ?>
			        <div class="row">
					<div class="center-block text-center footer-links" style="float:none;">
			            <div class="" id="footer-<?php echo $category ?>">
			              <?php foreach( $links as $link ): ?>
			              <div class="col-sm-3 col-md-3">
			                <div id="footer-<?php echo $category ?>-<?php echo $link ?>"><?php $this->html( $link ) ?></div>
			               </div>
			              <?php endforeach; ?>
			              <?php
			                if ($category === 'places') {
			
			                  # Show sign in link, if not signed in
			                  # don't show login here
			                  if (false && $wgBootstrapSkinLoginLocation == 'footer' && !$this->data['loggedin']) {
			                    $personalTemp = $this->getPersonalTools();
			
			                    if (isset($personalTemp['login'])) {
			                      $loginType = 'login';
			                    } else {
			                      $loginType = 'anonlogin';
			                    }
			
			                    ?><div class="col-md-3 col-sm-3" id="pt-login"><a href="<?php echo $personalTemp[$loginType]['links'][0]['href'] ?>"><?php echo $personalTemp[$loginType]['links'][0]['text']; ?></a></div><?php
			                  }
			
			                  # Show the search in footer to all
			                  if ($wgSearchPlacement['footer']) {
			                    echo '<div class="col-sm-3 col-md-3">';
			                    $this->renderNavigation( array( 'SEARCHFOOTER' ) ); 
			                    echo '</div>';
			                  }
			                }
			              ?>
			            </div>
			            </div>
			            </div>
			          <?php 
			          	  } // not info links
			              endforeach; 
			            } // has footer links
			          ?>

<div class="row">
		<div class="col-sm-6 col-md-6 icons-group center-block"  style="float:none;">
		<div class="text-center">
				<?php $footericons = $this->getFooterIcons( "icononly" );
				if ( count( $footericons ) > 0 ) {
					?>
					<ul id="footer-icons" class="list-inline">
						<?php
						foreach ( $footericons as $blockName => $footerIcons ) {
							?>
							<li id="footer-<?php
							echo htmlspecialchars( $blockName ); ?>ico">
								<?php
								foreach ( $footerIcons as $icon ) {
									?>
									<?php
									echo $this->getSkin()->makeFooterIcon( $icon );
									?>
	
								<?php
								}
								?>
							</li>
						<?php
						}
						?>
					</ul>
				<?php
				}
				?>
		</div>
		</div>
	</div>

<div class="row">
<div class="col-sm-6 col-md-6 center-block footer-links" style="float:none;">
		<!-- 
		<div>
			<h4>About</h4>
			<p>This is an encyclopedia site for adult knowledge, basically, anything related with SEX.</p>
		</div>
		-->
		<div class="footer-links text-center">
			Contact Us: 
		    <a href="http://www.twitter.com/xxxpedia_org" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');return false;">
			    <i class="fa fa-twitter" style="font-size: 20px;"></i>
		    </a>
			
			<!-- Others  -->
			<!-- 
		    <a href="https://www.facebook.com/sharer/sharer.php?u=http://www.xxxpedia.org/" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');return false;"><button type="button" class="btn btn-info btn-social"><i class="fa fa-facebook-square"></i></button></a>
			
			<a href="https://plus.google.com/share?url=http://www.xxxpedia.org/" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');return false;"><button type="button" class="btn btn-berry btn-social"><i class="fa fa-google-plus-square"></i></button></a>
			
		    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.xxxpedia.org/&amp;title=Mediawiki%20BootStrap%20|Skin&amp;summary=Mediawiki%20BootStrap%20Skin&amp;source=http://www.xxxpedia.org/" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');return false;"><button type="button" class="btn btn-cyanide btn-social"><i class="fa fa-linkedin-square"></i></button></a>
			
		    <a href="http://www.tumblr.com/share?v=3&amp;u=http%3A//www.xxxpedia.org/" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');return false;"><button type="button" class="btn btn-info btn-social"><i class="fa fa-tumblr-square"></i></button></a>	
		    -->
		    <!-- dont need this -->
		    <!-- 
		    <div style="margin-top:10px;">
					&copy; 2015 Xxxpedia.org
			</div>
			 -->
	    </div>
	</div>
</div>