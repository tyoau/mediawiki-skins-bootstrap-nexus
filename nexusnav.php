<?php if ( $wgGroupPermissions['*']['edit'] || $wgBootstrapSkinAnonNavbar || $this->data['loggedin'] ) { ?>
<div id="userbar" class="bg-cyan">
  <div class="body">
      <div class="col-md-12 hidden-xs hidden-sm">
      <ul class="nex-menu nex-menu-response-to-icons nex-menu-anim-slide">
	    <li><a href="<?php echo htmlspecialchars( $this->data['nav_urls']['mainpage']['href'] ) ?>"><i class="fa fa-home"></i> Main Page</a></li>
                <li aria-haspopup="true">
					<a href="#"><i class="fa fa-star"></i>Nexus</a>
					<div class="grid-container3">
						<ul>
							<li><a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Special:SpecialContact"><i class="fa fa-envelope"></i>Contact us</a></li>
							<li><a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Wiki_Demo"><i class="fa fa-pencil"></i>Wiki Demo</a></li>
                                                        <li><a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Site_Map"><i class="fa fa-globe"></i>Site Map</a>
									</li>
							<li aria-haspopup="true">
								<a href="#"><i class="fa fa-book"></i>Pages</a>
								<div class="grid-container3">
									<ul>
										<li>
											<a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Nexus_Features"><i class="fa fa-info"></i>Nexus Features</a>
										</li>
										<li>
											<a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Mobile_Viewing"><i class="fa fa-mobile"></i>Mobile Ready</a>
										</li>
										<li>
											<a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Social_Features"><i class="fa fa-facebook-square"></i>Social Plugins</a>
										</li>
										<li><a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Special:SpecialShop"><i class="fa fa-shopping-cart"></i>eCommerce</a>
									</li>
									<li><a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Nexus_Blog"><i class="fa fa-youtube-play"></i>Latest News</a>
									</li>
									</ul>
								</div>
							</li>
							<li aria-haspopup="true">
							<a href="#"><i class="fa fa-plus-square"></i>Commercial</a>
							<div class="grid-container3">
									<ul>
										<li>
											<a href="http://mediawikibootstrapskin.co.uk/nexus/index.php?title=Corporate_Services"><i class="fa fa-wrench"></i>Services</a>
										</li>
									</ul>
								</div>
							</li>							
						</ul>
					</div>
				</li>
                <li class="right search">					
					<form class="navbar-search" action="<?php $this->text( 'wgScript' ) ?>" id="searchform">					
						<div class="input">
							<button type="submit" class="button">Go</button>
							<input id="searchInput" type="search" accesskey="f" title="<?php $this->text('searchtitle'); ?>" placeholder="<?php $this->msg('search'); ?>" name="search" value="<?php echo htmlspecialchars ($this->data['search']); ?>">
						</div>					
					</form>
				</li>				
	
        <li><?php $this->renderNavigation( array( 'EDIT' ) );?></li>
		<li aria-haspopup="true"><?php $this->renderNavigation( array( 'PERSONALNAV' ) );?></li>
        <li aria-haspopup="true"><?php $this->renderNavigation( array( 'PAGE' ) );?></li>
		<li aria-haspopup="true"><?php $this->renderNavigation( array( 'ACTIONS' ) );?></li>
		<li aria-haspopup="true"><?php if ( !isset( $portals['TOOLBOX'] ) ) {$this->renderNavigation( array( 'TOOLBOX' ) );?></li>
      </ul> 
	  
        <?php
          if ( $wgBootstrapSkinLogoLocation == 'navbar' ) {
            $this->renderLogo();
          }
          # This content in other languages
          if ( $this->data['language_urls'] ) {
            $this->renderNavigation( array( 'LANGUAGES' ) );
          }

          # Sidebar items to display in navbar
          $this->renderNavigation( array( 'SIDEBARNAV' ) );
          }
		  
        ?>
		
      </div>
	  </div>

      <div class="pull-right">
        <?php
          if ($wgSearchPlacement['header']) {
            $this->renderNavigation( array( 'SEARCH' ) ); 
          }

          # Personal menu (at the right)
          # $this->renderNavigation( array( 'PERSONAL' ) ); 
        ?>
      </div>
  </div>
      <?php

      if ($wgSearchPlacement['nav']) {
        $this->renderNavigation( array( 'SEARCHNAV' ) );
      }

      ?>

      </ul>
 
</div>
<?php } ?>