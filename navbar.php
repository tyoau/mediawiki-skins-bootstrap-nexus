		<nav id="navbar" class="navbar navbar-nexus navbar-fixed-top <?php global $wgNavBarClasses; echo $wgNavBarClasses; ?>" role="navigation">
				<div class="container">
					<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
					<div class="navbar-header">
						<button class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo $this->data['nav_urls']['mainpage']['href'] ?>" title="<?php global $wgSitename; echo $wgSitename ?>"><?php global $wgNavLogo; echo isset( $wgNavLogo ) && $wgNavLogo ? "<img src='{$wgNavLogo}' alt='Logo'/> " : ''; /*echo $wgSitenameshort ?: $wgSitename; */?></a>
					</div>

					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav">
							<li aria-haspopup="true">
								<a href="<?php echo $this->data['nav_urls']['mainpage']['href'] ?>">Home</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo $url_prefix; ?>Special:RecentChanges" class="recent-changes"><i class="fa fa-edit"></i> Recent Changes</a></li>
									<li><a href="<?php echo $url_prefix; ?>Special:Random" class="random-articles"><i class="fa fa-random"></i> Random Articles</a></li>
									<li><a href="<?php echo $url_prefix; ?>Special:SpecialPages" class="special-pages"><i class="fa fa-star-o"></i> Special Pages</a></li>
									<?php global $wgEnableUploads; if ( $wgEnableUploads ) { ?>
									<li><a href="<?php echo $url_prefix; ?>Special:Upload" class="upload-a-file"><i class="fa fa-upload"></i> Upload a File</a></li>
									<?php } ?>
								</ul>
							</li>						
							<?php echo $this->nav( $this->get_page_links( 'Bootstrap:TitleBar' ) ); ?>
						</ul>
					<?php
					global $wgParser, $wgUser;
					if ( $wgUser->isLoggedIn() ) {
						if ( count( $this->data['personal_urls'] ) > 0 ) {
							$user_icon = '<span class="user-icon"><img src="https://secure.gravatar.com/avatar/'.md5(strtolower( $wgUser->getEmail())).'.jpg?s=20&r=g"/></span>';
							$name = strtolower( $wgUser->getName() );
							$user_nav = $this->get_array_links( $this->data['personal_urls'], $user_icon . $name, 'user' );
							?>
							<ul<?php $this->html('userlangattributes') ?> class="nav navbar-nav navbar-right">
								<?php echo $user_nav; ?>
							</ul>
							<?php
						}//end if

						if ( count( $this->data['content_actions']) > 0 ) {
							$content_nav = $this->get_array_links( $this->data['content_actions'], 'Page', 'page' );
							?>
							<ul class="nav navbar-nav content-actions"><?php echo $content_nav; ?></ul>
							<?php
						}//end if
					} else {  // else if is logged in
						?>
						<ul class="nav navbar-nav navbar-right">
							<li>
							<?php echo Linker::linkKnown( SpecialPage::getTitleFor( 'Userlogin' ), wfMsg( 'login' ) ); ?>
							</li>
						</ul>
						<?php
					}
					?>
					<form class="navbar-search navbar-form navbar-right" action="<?php $this->text( 'wgScript' ) ?>" id="searchform" role="search">
						<div>
							<input class="form-control" type="search" name="search" placeholder="Search" title="Search <?php echo $wgSitename; ?> [ctrl-option-f]" accesskey="f" id="searchInput" autocomplete="off">
							<input type="hidden" name="title" value="Special:Search">
						</div>
					</form>
					</div>
				</div>
		</nav><!-- topbar -->
