

#
# MUST disable the navbar css definition in MediaWiki:Common.css
# 

/*
/* Default styling for Navbar template */
.navbar {
    display: inline;
    font-size: 88%;
    font-weight: normal;
}
.navbar ul {
    display: inline;
    white-space: nowrap;
}
.mw-body-content .navbar ul {
    line-height: inherit;
}
.navbar li {
    word-spacing: -0.125em;
}
.navbar.mini li span {
  font-variant: small-caps;
}
*/