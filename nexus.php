<?php
/**
 * Initialisation file for the BootStrapSkin
 *
 * BootStrapSkin is a skin built on top of a modified Vector theme from
 * MediaWiki and utilises Bootstrap 3.1.1 for base layout,
 * typography, and additional widgets.
 * 
 * @file
 * @ingroup Skins
 * @authors Lee Miller
 */

if( !defined( 'MEDIAWIKI' ) ) die( "This is an extension to the MediaWiki package and cannot be run standalone." );
 
$wgExtensionCredits['skin'][] = array(
        'path' => __FILE__,
        'name' => 'Nexus',
        'url' => "http://www.tyo.com.au",
        'author' => 'TYO Lab and original authors (Lee Miller & Matthew Batchelder)',
        'descriptionmsg' => 'A skin adapt from both BootStrapSkin and BootstrapMediaWiki',
		'version' => '0.1',
);

$wgValidSkinNames['bootstrapnexus'] = 'Bootstrap';
$wgAutoloadClasses['SkinBootstrap'] = dirname(__FILE__).'/nexus.skin.php';
$wgExtensionMessagesFiles['SkinBootstrap'] = dirname(__FILE__).'/nexus.i18n.php';
 
$wgResourceModules['skins.bootstrapskin'] = array(
        'styles' => array(
        'nexus/bootstrap/css/bootstrap.min.css' => array( 'media' => 'screen' ),
		'nexus/bootstrap/css/bootstrap-theme.min.css' => array( 'media' => 'screen' ),
		'nexus/bootstrap/css/jquery-ui.min.css' => array( 'media' => 'screen' ),
		'nexus/bootstrap/css/font.css' => array( 'media' => 'screen' ),
        'nexus/bootstrap/css/typography.css' => array( 'media' => 'screen' ),
        'nexus/bootstrap/css/misc.css' => array( 'media' => 'screen' ),
// 		'nexus/bootstrap/css/social-buttons.css' => array( 'media' => 'screen' ),
        'nexus/bootstrap/css/font-awesome.min.css' => array( 'media' => 'screen' ), 
        'nexus/screen.css' => array( 'media' => 'screen' ),
        'nexus/theme.css' => array( 'media' => 'screen' ),
        'nexus/prefs.css' => array( 'media' => 'screen' ),
        'nexus/style.css' => array( 'media' => 'screen' ),
        'nexus/navbar.css' => array( 'media' => 'screen' ),        		
        ),

	'scripts' => array(
	    'nexus/bootstrap/js/jquery-ui.min.js',
	    'nexus/bootstrap/js/bootstrap.min.js',
	    'nexus/skin.js',
	    'nexus/bootstrap/js/buttons.js',
	    'nexus/bootstrap/js/misc.js',
        'nexus/bootstrap/js/timeline.js',
        'nexus/bootstrap/js/lazyYT.js',
        'nexus/bootstrap/js/share.min.js',
	),	
        'remoteBasePath' => &$GLOBALS['wgStylePath'],
        'localBasePath' => &$GLOBALS['wgStyleDirectory'],
);

# Default options to customise skin
$wgBootstrapSkinLogoLocation = 'bodycontent';
$wgBootstrapSkinLoginLocation = 'footer';
$wgBootstrapSkinAnonNavbar = true;
$wgBootstrapSkinUseStandardLayout = false;
$wgBootstrapSkinDisplaySidebarNavigation = false;
# Show print/export in navbar by default
$wgBootstrapSkinSidebarItemsInNavbar = array( 'coll-print_export' );

/*
$wgFooterIcons['poweredby']['bootstrapskin'] = array(
	      "src" => "http://www.mediawikibootstrapskin.co.uk/images/BootStrapSkin_mediawiki_88x31.png",
	      "url" => "http://www.mediawikibootstrapskin.co.uk/",
	      "alt" => "Powered by BootStrapSkin",
          );
		  
$wgFooterIcons['poweredby']['youricon'] = array(
	      "src" => "", //url to your wiki logo
	      "url" => "", //url to your wiki
	      "alt" => "", //alt text
          );
*/
