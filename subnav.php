	<?php
	if( $subnav_links = $this->get_page_links('Bootstrap:Subnav') ) {
		?>
		<div class="subnav subnav-fixed">
			<div class="container">
				<?php

				$subnav_select = $this->nav_select( $subnav_links );

				if ( trim( $subnav_select ) ) {

					?>
					<select id="subnav-select">
					<?php echo $subnav_select; ?>
					</select>
					<?php
				}//end if
				?>
				<ul class="nav nav-pills">
				<?php echo $this->nav( $subnav_links ); ?>
				</ul>
			</div>
		</div>
		<?php
	}//end if
	?>	